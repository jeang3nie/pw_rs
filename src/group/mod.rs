use {
    crate::Error,
    std::{
        fs::File,
        io::{BufRead, BufReader},
        str::FromStr,
    }
};
#[cfg(feature = "db")]
use std::fmt;
#[cfg(all(feature = "db", target_os = "linux"))]
#[path = "db/linux.rs"]
mod db;
#[cfg(all(
    feature = "db",
    any(target_os = "freebsd", target_os = "netbsd", target_os = "openbsd")
))]
#[path = "db/bsd.rs"]
mod db;
#[cfg(feature = "db")]
pub use db::Db;

pub static GROUPDB: &str = "/etc/group";
#[cfg(all(feature = "db", target_os = "linux"))]
pub static GSHADOWDB: &str = "/etc/gshadow";

/// An entry in /etc/group broken out into fields
#[derive(Debug)]
pub struct Group {
    /// The name of this group
    pub name: String,
    /// The (optional) hashed password of this group, useful
    /// for creating private groups
    pub pass: Option<String>,
    /// The gid of this group
    pub gid: u32,
    /// A list of the user names of the group's members
    pub members: Option<Vec<String>>,
}

#[cfg(feature = "db")]
impl fmt::Display for Group {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.pass {
            Some(p) => write!(f, "{}:{p}:{}:", self.name, self.gid)?,
            None => write!(f, "{}::{}:", self.name, self.gid)?,
        };
        if let Some(ref m) = self.members {
            write!(f, "{}", m.join(","))?;
        }
        Ok(())
    }
}

impl FromStr for Group {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.split(':');
        let Some(name) = it.next().map(ToString::to_string) else {
            return Err(Error::TruncatedLine);
        };
        let pass = match it.next() {
            Some(s) if s != "x" && !s.is_empty() => Some(s.to_string()),
            _ => None,
        };
        let gid = match it.next() {
            Some(g) => g.parse()?,
            None => return Err(Error::TruncatedLine),
        };
        let members = it.next().map(|x| {
            x.split(',')
                .map(ToString::to_string)
                .collect::<Vec<String>>()
        });
        Ok(Self {
            name,
            pass,
            gid,
            members,
        })
    }
}

impl Group {
    /// Gets a `Group` struct given a group's name
    /// # Errors
    /// If the group database file is inaccessible, will return an
    /// io error
    pub fn getgrnam(name: &str) -> Result<Option<Self>, Error> {
        let fd = File::open(GROUPDB)?;
        let reader = BufReader::new(fd);
        for line in reader.lines() {
            let line = line?;
            if line.starts_with('#') {
                continue;
            }
            let gr: Group = line.parse()?;
            if gr.name == name {
                return Ok(Some(gr));
            }
        }
        Ok(None)
    }

    /// Gets a `Group` struct given a group's gid number
    /// # Errors
    /// If the group database file is inaccessible, will return an
    /// io error
    pub fn getgrgid(gid: u32) -> Result<Option<Self>, Error> {
        let fd = File::open(GROUPDB)?;
        let reader = BufReader::new(fd);
        for line in reader.lines() {
            let line = line?;
            if line.starts_with('#') {
                continue;
            }
            let gr: Group = line.parse()?;
            if gr.gid == gid {
                return Ok(Some(gr));
            }
        }
        Ok(None)
    }
}
