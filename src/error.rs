use std::{fmt, io, num::ParseIntError};

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    EmptyPassword,
    ParseInt,
    UnknownUser,
    UnknownGroup,
    TruncatedLine,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Io(e) => write!(f, "{e}"),
            Self::EmptyPassword => write!(f, "Empty password"),
            Self::UnknownUser => write!(f, "Unknown user"),
            Self::UnknownGroup => write!(f, "Unknown group"),
            Self::TruncatedLine => write!(f, "Truncated line"),
            Self::ParseInt => write!(f, "Parse int error"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(e) => Some(e),
            _ => None,
        }
    }
}

impl From<io::Error> for Error {
    fn from(value: io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<ParseIntError> for Error {
    fn from(_value: ParseIntError) -> Self {
        Self::ParseInt
    }
}
