The `pw` crate is a Rust implementation of certain functionality centered
around users and groups in Unix, particularly the database files "/etc/passwd",
"/etc/group" and the associated **shadow** files, if applicable.

Contents
========
* [Introduction](#introduction)
* [Usage](#usage)
* [Features](#features)
* [Unsafe](#unsafe)

## Introduction
The Unix `/etc/passwd` file is a text only database file containig a list of
all users on the system. This file is relatively simple to parse, with each
line representing an individual user and colon separated fields. 

On Linux systems this file is "shadowed" and does not contain the actual
password hashes. The hashes are instead stored in '/etc/shadow', which can only
be read by root. On BSD systems there is instead an '/etc/master.passwd' file,
which has the hashes plus three extra fields which do not appear in Linux.

The '/etc/group' file is to groups what '/etc/passwd' is to users.

This crate breaks each user and group from these database files out into struct
fields in the `Passwd` and `Group` types, and contains methods for working with
that information.

## Usage
```Rust
let my_user = pw::Passwd::getpw().expect("Error getting user info").unwrap();
let my_groups = my_user.groups().expect("Error getting groups");
// Find if we're a member of "wheel"
if my_groups.any(|g| g.name == "wheel") {
    println!("Member of `wheel` group");
}
```

## Features
In order to get the current user's uid, effective uid, gid or effective gid this
crate can either make syscalls directly using the `sc` crate, or else do the exact
same thing indirectly by calling into libc using the `libc` crate. By default this
functionality is provided by direct syscalls. If desired, or necessary due to the
platform, then the crate can be compiled with `--no-default-features` and
`--features=libc`. This might be necessary for example if you are on FreeBSD aarch64,
which is not supported by the `sc` crate, or on OpenBSD where it is expected that
syscalls only come through libc.

## Unsafe
The getuid, geteuid, getgid and getegid functions all rely on syscalls, whether
directly made in Rust or by calling into libc. These calls are considered infallible
on every platform and the return value is unambiguous, therefore even though the
functions we wrap are technically `unsafe` the wrappers are safe Rust. These are the
only instances of `unsafe` in the crate. The rest of the crate was undertaken in order
to avoid using `unsafe` all over by using libc functions to parse the passwd and group
database files.
