use {
    super::{Error, Passwd, PASSDB, SHADOWDB},
    std::{
        fmt,
        fs::File,
        io::{BufRead, BufReader},
    },
};

#[derive(Debug)]
pub enum Entry {
    Comment(String),
    Error(String),
    Pw(Passwd),
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Comment(s) | Self::Error(s) => write!(f, "{s}"),
            Self::Pw(p) => write!(f, "{p}"),
        }
    }
}

impl From<&str> for Entry {
    fn from(s: &str) -> Self {
        if s.trim_start().starts_with('#') {
            Self::Comment(s.to_string())
        } else if let Ok(pw) = Passwd::from_master_line(s) {
            Self::Pw(pw)
        } else {
            Self::Error(s.to_string())
        }
    }
}

impl Entry {
    fn from_passwd_line(line: &str) -> Self {
        if line.trim_start().starts_with('#') {
            Self::Comment(line.to_string())
        } else if let Ok(pw) = Passwd::from_passwd_line(line) {
            Self::Pw(pw)
        } else {
            Self::Error(line.to_string())
        }
    }

    fn write_passwd(&self, f: &mut dyn fmt::Write) -> Result<(), fmt::Error> {
        match self {
            Self::Comment(s) | Self::Error(s) => write!(f, "{s}"),
            Self::Pw(pw) => pw.write_passwd_line(f),
        }
    }
}

#[derive(Debug)]
pub struct Db {
    entries: Vec<Entry>,
}

impl fmt::Display for Db {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for e in &self.entries {
            writeln!(f, "{e}")?;
        }
        Ok(())
    }
}

impl Db {
    /// Reads "/etc/master.passwd" into a `Db` struct. As "/etc/master.passwd"
    /// cannot be read by anyone other than the root user, this function should
    /// not be called by a normal user process.
    /// # Errors
    /// Returns error if "/etc/master.passwd" is inaccessible or unreadable.
    pub fn from_master_file() -> Result<Self, Error> {
        let fd = File::open(SHADOWDB)?;
        let reader = BufReader::new(fd);
        let mut db = Self { entries: vec![] };
        for line in reader.lines() {
            let line = line?;
            db.entries.push(line.as_str().into());
        }
        Ok(db)
    }

    /// Reads "/etc/passwd" into a `Db` struct. Unlike `from_master_file` this
    /// function can be called by a normal user process. However, the output will
    /// not contain password hashes, login classes or change and expiry dates.
    /// The output of this command therefore can not be used to recreate or modify
    /// the database files or critical login information will be lost.
    pub fn from_file() -> Result<Self, Error> {
        let fd = File::open(PASSDB)?;
        let reader = BufReader::new(fd);
        let mut db = Self { entries: vec![] };
        for line in reader.lines() {
            let line = line?;
            db.entries.push(Entry::from_passwd_line(&line));
        }
        Ok(db)
    }

    pub fn write_passwd(&self, f: &mut dyn fmt::Write) -> fmt::Result {
        for e in &self.entries {
            e.write_passwd(f)?;
            writeln!(f)?;
        }
        Ok(())
    }

    pub fn get(&self, name: &str) -> Option<&Passwd> {
        for e in &self.entries {
            match e {
                Entry::Pw(pw) if pw.name == name => return Some(pw),
                _ => continue,
            }
        }
        None
    }

    pub fn get_mut(&mut self, name: &str) -> Option<&mut Passwd> {
        for e in &mut self.entries {
            match e {
                Entry::Pw(ref mut pw) if pw.name == name => return Some(pw),
                _ => continue,
            }
        }
        None
    }
}
