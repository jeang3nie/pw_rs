#![allow(clippy::similar_names)]
use {
    crate::{group::GROUPDB, Error, Group},
    std::{
        fs::File,
        io::{BufRead, BufReader},
        string::ToString,
    },
};

#[cfg(feature = "db")]
use std::fmt;
#[cfg(feature = "db")]
mod db;
#[cfg(feature = "db")]
pub use db::Db;

static PASSDB: &str = "/etc/passwd";
static SHADOWDB: &str = "/etc/master.passwd";

/// An entry in /etc/passwd broken out into individual fields
#[derive(Debug)]
pub struct Passwd {
    /// The username associated with this user
    pub name: String,
    /// The hashed password of this user
    pub pass: Option<String>,
    /// The uid of this user
    pub uid: u32,
    /// The gid of this user's primary group
    pub gid: u32,
    pub class: Option<String>,
    pub change: Option<i64>,
    pub expire: Option<i64>,
    /// A comment field which usually contains the user's full name
    pub gecos: Option<String>,
    /// The user's $HOME directory
    pub home: String,
    /// The login shell of this user
    pub shell: Option<String>,
}

#[cfg(feature = "db")]
impl fmt::Display for Passwd {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:", self.name)?;
        match &self.pass {
            Some(p) => write!(f, "{p}:{}:{}:", self.uid, self.gid)?,
            None => write!(f, ":{}:{}:", self.uid, self.gid)?,
        }
        match &self.class {
            Some(c) => write!(f, "{c}:")?,
            None => write!(f, ":")?,
        }
        match self.change {
            Some(c) => write!(f, "{c}:")?,
            None => write!(f, ":")?,
        }
        match self.expire {
            Some(e) => write!(f, "{e}:")?,
            None => write!(f, ":")?,
        }
        match &self.gecos {
            Some(g) => write!(f, "{g}:{}:", self.home)?,
            None => write!(f, ":{}:", self.home)?,
        }
        if let Some(s) = &self.shell {
            write!(f, "{s}")?;
        }
        Ok(())
    }
}

impl Passwd {
    #[cfg(any(feature = "libc", feature = "syscall"))]
    /// Returns a `Passwd` struct for the current user
    /// # Errors
    /// If the passwd database file cannot be opened this function
    /// will return an io error. On some systems this function will
    /// only be callable by root due to permissions on /etc/passwd.
    /// If the passwd database file is broken, a parsing error will
    /// occur.
    pub fn getpw() -> Result<Option<Self>, Error> {
        let uid = crate::getuid();
        Self::getpwuid(uid)
    }

    #[cfg(any(feature = "libc", feature = "syscall"))]
    /// Returns a `Passwd` struct for the current effective user
    /// # Errors
    /// If the passwd database file cannot be opened this function
    /// will return an io error. On some systems this function will
    /// only be callable by root due to permissions on /etc/passwd.
    /// If the passwd database file is broken, a parsing error will
    /// occur.
    pub fn getepw() -> Result<Option<Self>, Error> {
        let euid = crate::geteuid();
        Self::getpwuid(euid)
    }

    fn from_master_line(line: &str) -> Result<Self, Error> {
        let mut it = line.splitn(10, ':');
        let Some(name) = it.next().map(ToString::to_string) else {
            return Err(Error::TruncatedLine);
        };
        let Some(pass) = it.next() else {
            return Err(Error::TruncatedLine);
        };
        let pass = if pass.is_empty() {
            None
        } else {
            Some(pass.to_string())
        };
        let uid: u32 = match it.next() {
            Some(s) => s.parse()?,
            None => return Err(Error::TruncatedLine),
        };
        let gid: u32 = match it.next() {
            Some(s) => s.parse()?,
            None => return Err(Error::TruncatedLine),
        };
        let mut class = it.next().map(ToString::to_string);
        if let Some(ref c) = class {
            if c.is_empty() {
                class = None;
            }
        }
        let change = if let Some(n) = it.next() {
            if n.is_empty() {
                None
            } else {
                Some(n.parse()?)
            }
        } else {
            None
        };
        let expire = if let Some(n) = it.next() {
            if n.is_empty() {
                None
            } else {
                Some(n.parse()?)
            }
        } else {
            None
        };
        let gecos = match it.next() {
            Some(g) if !g.is_empty() => Some(g.to_string()),
            _ => None,
        };
        let Some(home) = it.next().map(ToString::to_string) else {
            return Err(Error::TruncatedLine);
        };
        let mut shell = it.next().map(ToString::to_string);
        if let Some(ref s) = shell {
            if s.is_empty() {
                shell = None;
            }
        }
        Ok(Self {
            name,
            pass,
            uid,
            gid,
            class,
            change,
            expire,
            gecos,
            home,
            shell,
        })
    }

    fn from_passwd_line(line: &str) -> Result<Self, Error> {
        let mut it = line.splitn(7, ':');
        let Some(name) = it.next().map(ToString::to_string) else {
            return Err(Error::TruncatedLine);
        };
        let Some(_pass) = it.next() else {
            return Err(Error::TruncatedLine);
        };
        let uid: u32 = match it.next() {
            Some(s) => s.parse()?,
            None => return Err(Error::TruncatedLine),
        };
        let gid: u32 = match it.next() {
            Some(s) => s.parse()?,
            None => return Err(Error::TruncatedLine),
        };
        let gecos = match it.next() {
            Some(g) if !g.is_empty() => Some(g.to_string()),
            _ => None,
        };
        let Some(home) = it.next().map(ToString::to_string) else {
            return Err(Error::TruncatedLine);
        };
        let mut shell = it.next().map(ToString::to_string);
        if let Some(ref s) = shell {
            if s.is_empty() {
                shell = None;
            }
        }
        Ok(Self {
            name,
            pass: None,
            uid,
            gid,
            class: None,
            change: None,
            expire: None,
            gecos,
            home,
            shell,
        })
    }

    /// Returns a `Passwd` struct for the given uid
    /// # Errors
    /// If the passwd database file cannot be opened this function
    /// will return an io error. On some systems this function will
    /// only be callable by root due to permissions on /etc/passwd.
    /// If the passwd database file is broken, a parsing error will
    /// occur.
    pub fn getpwuid(uid: u32) -> Result<Option<Self>, Error> {
        let (euid, fd) = match File::open(SHADOWDB) {
            Ok(fd) => (0, fd),
            Err(_) => (1, File::open(PASSDB)?),
        };
        let reader = BufReader::new(fd);
        for line in reader.lines() {
            let line = line?;
            if line.trim_start().starts_with('#') {
                continue;
            }
            let pw: Passwd = if euid == 0 {
                Passwd::from_master_line(&line)?
            } else {
                Passwd::from_passwd_line(&line)?
            };
            if pw.uid == uid {
                return Ok(Some(pw));
            }
        }
        Ok(None)
    }

    /// Returns a `Passwd` struct for the given usename
    /// # Errors
    /// If the passwd database file cannot be opened this function
    /// will return an io error. On some systems this function will
    /// only be callable by root due to permissions on /etc/passwd.
    /// If the passwd database file is broken, a parsing error will
    /// occur.
    pub fn getpwnam(name: &str) -> Result<Option<Self>, Error> {
        let (euid, fd) = match File::open(SHADOWDB) {
            Ok(fd) => (0, fd),
            Err(_) => (1, File::open(PASSDB)?),
        };
        let reader = BufReader::new(fd);
        for line in reader.lines() {
            let line = line?;
            if line.trim_start().starts_with('#') {
                continue;
            }
            let pw: Passwd = if euid == 0 {
                Passwd::from_master_line(&line)?
            } else {
                Passwd::from_passwd_line(&line)?
            };
            if pw.name == name {
                return Ok(Some(pw));
            }
        }
        Ok(None)
    }

    /// Returns the list of groups that this user is a member of
    /// # Errors
    /// If the group database file (/etc/group) cannot be opened and
    /// read then this function will return an io error.
    pub fn groups(&self) -> Result<Vec<Group>, Error> {
        let fd = File::open(GROUPDB)?;
        let reader = BufReader::new(fd);
        let mut groups = vec![];
        for line in reader.lines() {
            let line = line?;
            if line.trim_start().starts_with('#') {
                continue;
            }
            let grp: Group = line.parse()?;
            if let Some(ref mem) = grp.members {
                if mem.contains(&self.name) {
                    groups.push(grp);
                }
            } else if grp.gid == self.gid {
                groups.push(grp);
            }
        }
        Ok(groups)
    }

    #[cfg(feature = "db")]
    fn write_passwd_line(&self, f: &mut dyn fmt::Write) -> fmt::Result {
        write!(f, "{}:*:{}:{}:", self.name, self.uid, self.gid)?;
        match &self.gecos {
            Some(g) => write!(f, "{g}:{}:", self.home)?,
            None => write!(f, ":{}:", self.home)?,
        }
        if let Some(s) = &self.shell {
            write!(f, "{s}")?;
        }
        Ok(())
    }
}
