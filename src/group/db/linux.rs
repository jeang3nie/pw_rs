use {
    super::{Error, GSHADOWDB},
    std::{ fmt, fs::File, io::{self, BufRead, BufReader}, str::FromStr},
};

#[derive(Debug)]
pub struct GShadow {
    pub name: String,
    pub pass: Option<String>,
    pub administrators: Option<Vec<String>>,
    pub members: Option<Vec<String>>,
}

impl fmt::Display for GShadow {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.pass {
            Some(ref p) => write!(f, "{}:{p}:", self.name)?,
            None => write!(f, "{}::", self.name)?,
        }
        match self.administrators {
            Some(ref a) => write!(f, "{}:", a.join(","))?,
            None => write!(f, ":")?,
        }
        if let Some(ref m) = self.members {
            write!(f, "{}", m.join(","))?;
        }
        Ok(())
    }
}

impl FromStr for GShadow {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.split(':');
        let Some(name) = it.next().map(ToString::to_string) else {
            return Err(Error::TruncatedLine);
        };
        let pass = match it.next() {
            Some(s) if s != "x" && !s.is_empty() => Some(s.to_string()),
            _ => None,
        };
        let administrators = it.next().map(|a| {
            a.split(',')
                .map(ToString::to_string)
                .collect::<Vec<String>>()
        });
        let members = it.next().map(|x| {
            x.split(',')
                .map(ToString::to_string)
                .collect::<Vec<String>>()
        });
        Ok(Self {
            name,
            pass,
            administrators,
            members,
        })
    }
}

#[derive(Debug)]
pub enum Entry {
    Comment(String),
    Error(String),
    Grp(GShadow),
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Comment(s) | Self::Error(s) => write!(f, "{s}"),
            Self::Grp(gr) => write!(f, "{gr}"),
        }
    }
}

impl From<&str> for Entry {
    fn from(s: &str) -> Self {
        if s.trim_start().starts_with('#') {
            Self::Comment(s.to_string())
        } else if let Ok(gr) = s.parse() {
            Self::Grp(gr)
        } else {
            Self::Error(s.to_string())
        }
    }
}

#[derive(Debug)]
pub struct Db {
    entries: Vec<Entry>,
}

impl fmt::Display for Db {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for e in &self.entries {
            writeln!(f, "{e}")?;
        }
        Ok(())
    }
}

impl Db {
    pub fn from_file() -> Result<Self, io::Error> {
        let fd = File::open(GSHADOWDB)?;
        let reader = BufReader::new(fd);
        let mut db = Self { entries: vec![] };
        for line in reader.lines() {
            let line = line?;
            db.entries.push(line.as_str().into());
        }
        Ok(db)
    }

    pub fn get(&self, name: &str) -> Option<&GShadow> {
        for e in &self.entries {
            match e {
                Entry::Grp(gr) if gr.name == name => return Some(gr),
                _ => continue,
            }
        }
        None
    }

    pub fn get_mut(&mut self, name: &str) -> Option<&mut GShadow> {
        for e in &mut self.entries {
            match e {
                Entry::Grp(ref mut gr) if gr.name == name => return Some(gr),
                _ => continue,
            }
        }
        None
    }
}
