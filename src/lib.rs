#![warn(clippy::all, clippy::pedantic)]
#![allow(clippy::must_use_candidate)]

mod error;
pub(crate) mod group;

#[cfg(target_os = "linux")]
#[path = "passwd/linux/mod.rs"]
mod passwd;

#[cfg(any(target_os = "freebsd", target_os = "netbsd", target_os = "openbsd"))]
#[path = "passwd/bsd/mod.rs"]
mod passwd;

pub use {error::Error, group::Group, passwd::Passwd};

#[cfg(feature = "db")]
pub use {group::Db as GroupDb, passwd::Db as PasswdDb};

#[cfg(all(feature = "syscall", feature = "libc"))]
compile_error!("Feature \"syscall\" and feature \"libc\" cannot be enabled at the same time");

#[cfg(feature = "syscall")]
#[path = "uidgid/syscall.rs"]
mod uidgid;

#[cfg(feature = "libc")]
#[path = "uidgid/libc.rs"]
mod uidgid;

#[cfg(any(feature = "libc", feature = "syscall"))]
pub use uidgid::{getegid, geteuid, getgid, getuid};

#[cfg(any(feature = "libc", feature = "syscall"))]
/// Gets the list of groups to which the current user belongs
/// # Errors
/// If the passwd or group database files are inaccessible to the
/// current user this function will return an io error.
pub fn get_groups() -> Result<Vec<Group>, Error> {
    get_groups_for_uid(getuid())
}

/// Returns a list of groups for the user who's name is `name`,
/// if such a user exists
/// # Errors
/// If the passwd or group database files are inaccessible or broken,
/// this function will return an error. On some systems, these files
/// will have permissions set such that only root will be able to call
/// this function.
pub fn get_groups_for_name(name: &str) -> Result<Vec<Group>, Error> {
    let Some(pw) = Passwd::getpwnam(name)? else {
        return Err(Error::UnknownUser);
    };
    pw.groups()
}

/// Returns a list of groups for the given uid, if such a user exists
/// on this system
/// # Errors
/// If the passwd or group database files are inaccessible or broken,
/// this function will return an error. On some systems, these files
/// will have permissions set such that only root will be able to call
/// this function.
pub fn get_groups_for_uid(uid: u32) -> Result<Vec<Group>, Error> {
    let Some(pw) = Passwd::getpwuid(uid)? else {
        return Err(Error::UnknownUser);
    };
    pw.groups()
}
