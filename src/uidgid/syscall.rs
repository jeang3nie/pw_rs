#![allow(clippy::cast_possible_truncation, clippy::must_use_candidate)]
use sc::syscall;

/// Returns the uid of the calling process
pub fn getuid() -> u32 {
    unsafe { syscall!(GETUID) as u32 }
}

/// Returns the effective uid of the calling process
pub fn geteuid() -> u32 {
    unsafe { syscall!(GETEUID) as u32 }
}

/// Returns the gid of the calling process
pub fn getgid() -> u32 {
    unsafe { syscall!(GETGID) as u32 }
}

/// Returns the effective gid of the calling process
pub fn getegid() -> u32 {
    unsafe { syscall!(GETEGID) as u32 }
}
