use {
    super::{Group, GROUPDB},
    std::{ fmt, fs::File, io::{self, BufRead, BufReader}}
};

#[derive(Debug)]
pub enum Entry {
    Comment(String),
    Error(String),
    Grp(Group),
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Comment(s) | Self::Error(s) => write!(f, "{s}"),
            Self::Grp(gr) => write!(f, "{gr}"),
        }
    }
}

impl From<&str> for Entry {
    fn from(s: &str) -> Self {
        if s.trim_start().starts_with('#') {
            Self::Comment(s.to_string())
        } else if let Ok(gr) = s.parse() {
            Self::Grp(gr)
        } else {
            Self::Error(s.to_string())
        }
    }
}

#[derive(Debug)]
pub struct Db {
    entries: Vec<Entry>,
}

impl fmt::Display for Db {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for e in &self.entries {
            writeln!(f, "{e}")?;
        }
        Ok(())
    }
}

impl Db {
    pub fn from_file() -> Result<Self, io::Error> {
        let fd = File::open(GROUPDB)?;
        let reader = BufReader::new(fd);
        let mut db = Self { entries: vec![] };
        for line in reader.lines() {
            let line = line?;
            db.entries.push(line.as_str().into());
        }
        Ok(db)
    }

    pub fn get(&self, name: &str) -> Option<&Group> {
        for e in &self.entries {
            match e {
                Entry::Grp(gr) if gr.name == name => return Some(gr),
                _ => continue,
            }
        }
        None
    }

    pub fn get_mut(&mut self, name: &str) -> Option<&mut Group> {
        for e in &mut self.entries {
            match e {
                Entry::Grp(ref mut gr) if gr.name == name => return Some(gr),
                _ => continue,
            }
        }
        None
    }
}
