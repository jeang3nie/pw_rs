#![allow(clippy::must_use_candidate)]

/// Returns the uid of the calling process
pub fn getuid() -> u32 {
    unsafe { libc::getuid() }
}

/// Returns the effective uid of the calling process
pub fn geteuid() -> u32 {
    unsafe { libc::geteuid() }
}

/// Returns the gid of the calling process
pub fn getgid() -> u32 {
    unsafe { libc::getgid() }
}

/// Returns the effective gid of the calling process
pub fn getegid() -> u32 {
    unsafe { libc::getegid() }
}
